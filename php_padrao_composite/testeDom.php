<?php

	ini_set('display_errors', 'on');
	error_reporting(E_ALL);

	// O separador de diretórios utilizado pelo SO onde o script irá rodar
	$DS = DIRECTORY_SEPARATOR;

	include_once( dirname( __FILE__ ) . "{$DS}dom{$DS}Atributo.php");
	include_once( dirname( __FILE__ ) . "{$DS}dom{$DS}Elemento.php");

	// Nó raiz
	$noHtml = new Elemento( "html", array(), "", "R" );
	
	$noHead = new Elemento( "head", array(), "", "R" );

	$noTitle = new Elemento( "title", array(), "Padr&atilde;o Composite", "R" );
	
	$noHead->setElementos( array( $noTitle ) );
	
	// Corpo
	$noBody = new Elemento( "body", array(), "", "R" );
	
	$noHtml->setElementos( array( $noHead, $noBody ) );
	
	$noHtml->compor();
	
	print $noHtml->getComposicao(); 
?>