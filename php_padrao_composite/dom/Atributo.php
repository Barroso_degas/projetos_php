<?php

	/*
	 * Classe que representa os atributos de um elemento/nó 
	 */
	class Atributo {
		
		private $nome;
		private $valores;
		private $separaValor = ' '; // O tipo de separador a ser utilizados para os atributos que contiverem mais que um valor
		
		public function __construct( $nome, array $valores = array() ) {

			if( trim( $nome ) !== '' && is_string( $nome ) )
				$this->nome = $nome; 
			else
				throw new Exception("Nome invalido: {$nome}");

			
				
			foreach ( $valores as $valor ) {

				if( trim( $valor ) !== '' )
					$this->valores[] = $valor;
				else
					throw new Exception("Valor invalido: {$valor}");
			}
		}
		
		public function getNome() {
			return $this->nome;
		}
		public function setNome( string $nome ) {

			if( trim( $nome ) !== '' && is_string( $nome ) )
				$this->nome = $nome; 
			else
				throw new Exception("Nome invalido: {$nome}");
		}
		
		public function getValores() {
			return $this->valores;
		}
		public function addValores( array $valores ) {
			
			foreach ( $valores as $valor )
				$this->valores[] = $valor;
		}

		public function removeValor( string $key ) {
			
			if( isset( $this->valores[$key] ) ) {
				
				unset( $this->valores[$key] );
				
				$this->valores = array_values( $this->valores );
			}
			else {
				throw new Exception("Chave: {$key}, inexistente.");
			}
		}
		
		public function clearValores() {
			
			unset($this->valores);
			$this->valores = array();
		}
		
		public function getSeparaValor() {
			 return $this->separaValor;
		}
		
		public function setSeparaValor( $separador ) {
			 return $this->separaValor = $separador;
		}
	}

?>