<?php

	include_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'ElementoAbstract.php' );
	
	/*
	 * Classe que pode representar um nó raiz e/ou um nó folha na árvore DOM
	 */
	class Elemento extends ElementoAbstract {

		public function __construct( $nome, array $atributos, $valor, $tipo ) {

			parent::__construct( $nome, $atributos, array(), $valor, $tipo );
		}
		
		public function getElementos() {

			if( parent::getTipo() === 'F' )
				throw new Exception( 'Metodo nao pode ser implimentado por um elemento folha.' );
			else
				return parent::getElementos();
		}
		public function setElementos( array $elementos ) {

			if( parent::getTipo() === 'F' )
				throw new Exception( 'Metodo nao pode ser implimentado por um elemento folha.' );
			else
				parent::setElementos( $elementos );
		}
		
		public function getElemento( string $key ) {

			if( parent::getTipo() === 'F' )
				throw new Exception( 'Metodo nao pode ser implimentado por um elemento folha.' );
			else
				return parent::getElemento( $key );	
		}

		public function removeElemento( string $key ) {

			if( parent::getTipo() === 'F' )
				throw new Exception( 'Metodo nao pode ser implimentado por um elemento folha.' );
			else
				parent::removeElemento( $key );	
		}
		
		public function clearElementos() {

			if( parent::getTipo() === 'F' )
				throw new Exception( 'Metodo nao pode ser implimentado por um elemento folha.' );
			else
				parent::clearElementos();	
		}
		
		/*
		 * Por se tratar de um nó folha, este método é responsável por fazer a montagem 
		 * do último elemento de cada fluxo na árvore DOM. 
		 * Faz com que ela se tenha dos elementos criados e o retorna em uma string formatada.
		 */
		public function compor() {
			
			// Inicia o elemento desta composição
			$this->composicao .= $this->tabulaLinha . "<" . parent::getNome();
			
			// Adiciona os atributos encontrados
			foreach ( parent::getAtributos() as $atributo ) {

				$this->composicao .= ' "' . $atributo->getNome() . '"=';
				
				// Adiciona os valores encontrados para cada atributo
				if( count( $atributo->getValores() ) > 1 ) {

					$this->composicao .= '"';
					
					foreach ( $atributo->getValores() as $valor ) {
						
						if( is_string( $valor ) )
							$this->composicao .= $atributo->getSeparaValor() . $valor;
						else 
							$this->composicao .= $atributo->getSeparaValor() . $valor;
					}
					
					$this->composicao .= '"';
				}
				else {
					
					foreach ( $atributo->getValores() as $valor ) {
						
						if( is_string( $valor ) )
							$this->composicao .= '"' . $valor . '"';
						else 
							$this->composicao .= $valor;
					}
				}
			}
			
			$this->composicao .= ">" . $this->quebraLinha;
			
			// Inicia o próximo elemento, se houver, no elemento desta composição.
			if( count( parent::getElementos() ) > 0 ) {
				
				foreach( parent::getElementos() as $elemento ) 
					$this->composicao .= $elemento->compor();
			}

			// Adiciona o valor ao elemento
			$this->composicao .= $this->tabulaLinha . $this->tabulaLinha . parent::getValor() . $this->quebraLinha;
			
			// Encerra o elemento da composição
			$this->composicao .= $this->tabulaLinha . "</" . parent::getNome() . ">" . $this->quebraLinha;
			
			return $this->composicao;
		}
	}

?>