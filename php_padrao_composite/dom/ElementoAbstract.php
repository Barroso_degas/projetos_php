<?php

	include_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'Elemento.php' );
	include_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'Atributo.php' );

	/*
	 * Class abstrata da composição
	 * 
	 * Esta classe mantem os dados que serão manipulado pelo nó raiz ou 
	 * pelo nó folha da composição da árvore DOM
	 */
	class ElementoAbstract {

		private $nome;
		private $atributos;
		private $elementos;
		private $valor;
		private $tipo; // R = Raiz ou F = Folha
		
		protected $composicao = '';
		protected $quebraLinha = "\n";
		protected $tabulaLinha = "\t";
		
		public function __construct( $nome, array $atributos, array $elementos, $valor, $tipo ) {

			$this->atributos = array();
			$this->elementos = array();
			
			if( trim( $nome ) !== '' && is_string( $nome ) )
				$this->nome = $nome; 
			else
				throw new Exception("Nome invalido: {$nome}");
			
				
			foreach ( $atributos as $atributo ) {

				if( is_object( $atributo ) )
					$this->atributos[] = $atributo;
				else
					throw new Exception("Atributo invalido: {$atributo}");
			}
			
			
			foreach ( $elementos as $elemento ) {

				if( is_object( $elemento ) )
					$this->elementos[] = $elemento;
				else
					throw new Exception("Elemento invalido: {$elemento}");
			}
			
			if( $tipo !== 'R' && $tipo !== 'F' )
				throw new Exception("Tipo invalido: {$elemento}. O tipo de elemento deve ser: R = Raiz ou F = Folha");
			else 
				$this->tipo = $tipo;
			
			$this->valor = $valor;
		}
		
		public function getNome() {
			return $this->nome;
		}
		public function setNome( string $nome ) {

			if( trim( $nome ) !== '' )
				$this->nome = $nome; 
			else
				throw new Exception("Nome invalido: {$nome}");
		}
		
		public function getAtributos() {
			return $this->atributos;
		}
		public function setAtributos( array $atributos ) {
			
			foreach ( $atributos as $atributo ) {

				if( is_object( $atributo ) )
					$this->atributos[] = $atributo;
				else
					throw new Exception("Atributo invalido: {$atributo}");
			}
		}
		
		public function getAtributo( string $key ) {
			
			if( isset( $this->atributos[$key] ) ) {
				
				return $this->atributos[$key];
			}
			else {
				throw new Exception("Atributo: {$key}, inexistente.");
			}
		}

		public function removeAtributo( string $key ) {
			
			if( isset( $this->atributos[$key] ) ) {
				
				unset( $this->atributos[$key] );
				
				$this->atributos = array_values( $this->atributos );
			}
			else {
				throw new Exception("Atributo: {$key}, inexistente.");
			}
		}
		
		public function clearAtributos() {
			
			unset($this->atributos);
			$this->atributos = array();
		}
		
		public function getElementos() {
			return $this->elementos;
		}
		public function setElementos( array $elementos ) {
			
			foreach ( $elementos as $elemento ) {

				if( is_object( $elemento ) )
					$this->elementos[] = $elemento;
				else
					throw new Exception("Elemento invalido: {$elemento}");
			}
		}
		
		public function getElemento( string $key ) {
			
			if( isset( $this->elementos[$key] ) ) {
				
				return $this->elementos[$key];
			}
			else {
				throw new Exception("Elemento: {$key}, inexistente.");
			}
		}

		public function removeElemento( string $key ) {
			
			if( isset( $this->elementos[$key] ) ) {
				
				unset( $this->elementos[$key] );
				
				$this->elementos = array_values( $this->elementos );
			}
			else {
				throw new Exception("Elemento: {$key}, inexistente.");
			}
		}
		
		public function clearElementos() {
			
			unset($this->elementos);
			$this->elementos = array();
		}
		
		public function getValor() {
			return $this->valor;
		}
		public function setValor( $valor ) {

			$this->valor = $valor; 
		}
		
		public function getTipo() {
			return $this->tipo;
		}
		public function setTipo( $tipo ) {

			$this->tipo = $tipo; 
		}
		
		public function getComposicao() {
			
			return $this->composicao;
		}
		
		/*
		 * Método implemtado apenas pelas classes que herdarem desta classe abstrata
		 */
		public function compor() {
			
			throw new Exception( 'Metodo nao implimentado pelo elemento abstrato.' );
		}
	}

?>